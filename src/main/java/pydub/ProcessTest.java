package pydub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProcessTest {
	public static void main(String[] args) {
		ProcessBuilder pb = new ProcessBuilder();
		try {
			//String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class); 
			Process p = pb.command("E:\\tools\\dist\\ffmpeg.exe","-i",
					 "E:\\code\\git_work\\ttskit-main\\ttskit\\resource\\audio\\biaobei-biaobei-009502.mp3").start();
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream(),"UTF-8"));
			String s = "";
			while((s=br.readLine())!= null){
				System.out.println(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
 
}
